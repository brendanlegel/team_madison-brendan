
"""#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#Goal:Shorten Data

file = open("new_data[1000000].txt", "w")             #change [] for accurate title
with open('train_triplets.txt') as input_file:
    i = 0
    for line in input_file:     
        i+=1
        file.write(line)
        if i==1000000:                                #change [] for increased source size
            break
file.close()
print("File Sliced")

#Shortens 2.5 GB file to 2 MB slice, size changable if person wants more
hot fix in order to access file/ create it for user /vb 
#************************************************************
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#Goal:format data

user_database = {}                                  #complete Database dic with users
with open("new_data[1000000].txt") as input_stream:   #import code change [] to correct source
    i = 0
    j = 0
    k = 0
    userCount = 0
    for line in input_stream:
        storage = line
        test = storage[0:40]                        #spot hard check coded for exact keygen size 
        song = storage[41:59]                       #grab song piece of transaction
        if j == test:
            k += 1
            if k < 50:
                old = user_database[j]
                new = song
                user_database.update({j:old + " " + new})
                j = storage[0:40]
        else:
            k = 0
            user_database[test] = song
            j = storage[0:40]
            userCount += 1                          #correctly removes user and creates song dict

print("File Edited")
hot fix 2: fix duplicates, recover proper transaction formatting and dict out variables 
#************************************************************
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#Goal:recreate data set useable

with open("processed_data[1000000].txt", "w") as file:  #change for utility 
    for key in user_database:                       #stored in [song][song][song]...
        line = user_database[key]
        file.write(line+"\n")
print("File Processed")
hot fix 3: creating a working text file in format to utilize and check results       """
#************************************************************
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#Goal:create Apriori Algorithm 
#assistance sources listed in comments
#main assistance and credit go to: 
#1)http://blog.derekfarren.com/2015/02/how-to-implement-large-scale-market.html
#2)http://vipulchaskar.blogspot.com/2014/02/implementation-of-apriori-algorithm.html
#3)https://en.wikipedia.org/wiki/Apriori_algorithm

#------------------------------------------------------------ Parameters to change
support = 250
confidence_interval = float(3/10)
print("Parameters Set")
#------------------------------------------------------------
#<File imports HERE> )))))))))))))))))))))))))))))))))))))))))
import urllib
from itertools import combinations
from collections import defaultdict
from distutils.core import setup
import py2exe
#)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))                  #START LINE TRACK HERE FOR TOTAL LINE SIZE MAX 75

#set work file
work_file = "processed_data[1000000].txt"                   #CHANGE [] HERE FOR DIF SIZE
#first check function
def check(work_file, support, dmass, n):                   #dmass & n define later
    with open(work_file, 'r') as open_file:                #dmass === full candidate dictionary mass holder to edit and move
        for line in open_file:                             #n === iteration number for tracking checks
            listI = line.split()                           #track items in line
            dmass = update_dmass(listI, dmass, n)          #pass check to update function
    dmass = clear(dmass, support, n)                       #clear/purge unsupported frequency/ songs from the list 
    return dmass                                           #returns dmass purged of non supported and updated accordingly with check
    

#***************************************************************
def update_dmass(listI, dmass, n):                              #updates the array of all the transaction 
    if n == 1:                                                  #check the item list and push it over 
        for item in listI:                                      #goes through the txt lines import and puts into te dmass dictionary 
            dmass[(item,)]+=1                                   
    else:
        frequent_set = set()                                    #init
        for item_tuple in combinations(sorted(listI),n-1):      #sort through list and creating a count for the amount
            if item_tuple in dmass:           
                frequent_set.update(item_tuple)                 #creating a loop to remove duplicates 
                
        for item_set in combinations(sorted(frequent_set), n):  
            dmass[item_set]+=1
            
    return dmass
#***************************************************************
def clear(dmass, support, n):                           # clears all those who don't satisfy the support
    for item_tuple, count in dmass.items():                     #creates loop for checking each item 
        if count<support or len(item_tuple)<n:              # points out items that don't meet the crit
            del dmass[item_tuple]                       #removes those items from the group
    return dmass                                        #returns the cleaned set of data 


    
#***************************************************************
def main(work_file, support, set_Size):                     # cbring everything together ;;;; takes file, support, and the basket set size
    dmass = defaultdict(int)                                #initializes the dictionary mass set 
    for i in range(set_Size):                               # for a in all range ie size 3 or 2 or 1 do
        dmass = check(work_file, support, n = i+1, dmass=dmass) # initiate a check sweep 
    return dmass                                            #the final printe set dictionary containing the freq and transaction basket 

"""#***************************************************************
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#Goal: Create a rule maker using the above output data from dmass 
    #online video said to push multiple rule sets : https://www.youtube.com/watch?v=TcUlzuQ27iQ 
    #thanks Laurel :( """


t_Size = 3

t_dct = main(work_file, support, t_Size)
i=0
for t_set, freq in t_dct.iteritems():                 #3 size set baskets of significants
    print t_set, freq
    i+=1
    

t_Size = 2                                            #2 size set baskets of significants
t_dct = main(work_file, support, t_Size)
i=0
for t_set, freq in t_dct.iteritems():
    print t_set, freq
    i+=1
    

t_Size = 1                                            #1 size set basket of significants 
t_dct = main(work_file, support, t_Size)
i=0
for t_set, freq in t_dct.iteritems():
    print t_set, freq
    i+=1 

